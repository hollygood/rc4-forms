<?php

App::uses('AppModel', 'Model');

class UserForm extends AppModel {

  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'CustomForm', 'User'
  );
  
  public $hasMany = array(
    'UserFormValue' => array(
      'dependent' => true,
    )
  );

}