<?php

App::uses('AppModel', 'Model');

class UserFormValue extends AppModel {

  public $actsAs = array('Containable');

  public $belongsTo = array(
    'UserForm',
    'CustomFormField'
  );
  
  public function afterFind($results, $primary = false) {
    
    foreach($results as $key => $result) {
      $form_field = $this->CustomFormField->findById($result['UserFormValue']['custom_form_field_id']);
      
      if($form_field['CustomFormField']['type'] == 'checkboxes') {
        $checkboxes = json_decode($result['UserFormValue']['value']);
        
        $results[$key]['UserFormValue']['value'] = array();
        
        foreach($checkboxes as $checkbox)
          $results[$key]['UserFormValue']['value'][$checkbox] = $checkbox;
      }
      
    }
    
    return $results;
  }
  
}