<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	
	public $actsAs = array('Containable');
	
	public $hasMany = array(
		'CustomForm', 'UserForm'
	);
	
	public $validate = array(
		'first_name' => array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'allowEmpty' => false,
				'message' => 'Please provide a first name'
			)
		),
		'last_name' => array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'allowEmpty' => false,
				'message' => 'Please provide a last name'
			)
		),
		'email' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'This email is already in use'				
			),
			'email' => array(
				'rule' => 'email',
				'message' => 'Please provide a valid email'
			)
		),
		'password' => array(
			'minSix' => array(
				'rule' => array('minLength', 6),
				'message' => 'Password must be minimum 6 characters'
			)
		),
			
		'password2' => array(
			'identical' => array(
				'rule' => array('confirmCheck', 'password'),
				'message' => 'Passwords do not match'
			)
		)	
	);
	
	public function confirmCheck($field = array(), $compare_field = null) {
		
		foreach( $field as $key => $value ){
			$v1 = $value;
			$v2 = $this->data[$this->name][ $compare_field ];
			
			if ($v1 !== $v2)
				return false;
		}
		return true; 
	}			
	
	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$passwordHasher = new SimplePasswordHasher();
			$this->data[$this->alias]['password'] = $passwordHasher->hash(
				$this->data[$this->alias]['password']
			);
		}
		return true;
	}
	
}
