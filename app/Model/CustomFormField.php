<?php

App::uses('AppModel', 'Model');

class CustomFormField extends AppModel {
	
	public $actsAs = array('Containable');
	
	public $belongsTo = array(
		'CustomForm' => array(
			'className' => 'CustomForm',
			'foreignKey' => 'custom_form_id'
		)
	);
	
	public $hasMany = array('UserFormValue');
	
	public function types() {
		$types = array(
			'text' => 'Single-Line Text',
			'textarea' => 'Multi-Line Text',
			'select' => 'Dropdown Menu',
			'checkbox' => 'Checkbox',
			'checkboxes' => 'Multiple Checkboxes',
			'radio' => 'Radio Buttons',
			'email' => 'Email',
			'tel' => 'Telephone'
		);
		
		return $types;
	}
	
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => 'notEmpty',
				'message' => 'Please provide a question'
			)
		),
		'type' => array(
			'inList' => array(
				'rule' => array('inList', array('text','textarea','select','checkbox','checkboxes','radio','email','tel')),
				'message' => 'Please pick from the list',
			)
		)
	);
	
	public function afterFind($results, $primary = false) {
		$types = $this->types();
		
		
		foreach($results as $key => $result) {
			$type = $results[$key]['CustomFormField']['type'];
			
			$results[$key]['CustomFormField']['type_label'] = $types[$type];

			if ($type == 'select' || $type == 'radio' || $type == 'checkboxes') {
				$options = explode(PHP_EOL, $results[$key]['CustomFormField']['options']);
				$a = array();
				
				foreach($options as $option) {
					$a = array_merge($a, array($option => $option));
				}
				
				$results[$key]['CustomFormField']['options_array'] = $a;
				
			}
			
		}
		return $results;
	}
	
	
}

?>