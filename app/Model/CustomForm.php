<?php

App::uses('AppModel', 'Model');

class CustomForm extends AppModel {
	
	public $actsAs = array('Containable');
	
	public $belongsTo = array(
		'User'
	);
	
	public $hasMany = array(
		'CustomFormField', 'UserForm'
	);
	
	public $virtualFields = array(
		'user_forms_count' => ' SELECT COUNT(*) FROM user_forms UserForm WHERE UserForm.custom_form_id = CustomForm.id ',
		'last_modified' => ' SELECT MAX(modified) FROM user_forms UserForm WHERE UserForm.custom_form_id = CustomForm.id '
	);	
	
	public function canEdit($customForm, $user) {
		
		if (($user['role'] == 'A') || ($customForm['CustomForm']['user_id'] == $user['id'])) {
			return true;
		}
		return false;
	}
	
	public $validate = array(
		'name' => array(
			'alphaNumeric' => array(
				'rule' => 'notEmpty',
				'allowEmpty' => false,
				'message' => 'Please provide a name'
			)
		),
	);
	
	public function status() {
		return array(
			'Active' => 'Active',
			'Inactive' => 'Inactive'
		);
	}

	
}

?>