<?php

App::uses('FormHelper', 'View/Helper');

class BootstrapFormHelper extends FormHelper {
	public $helpers = array('Html', 'Js');
	
	public function input($fieldName, $options = array()) {
		
		//-----------------------------------------------------------TOOLTIPS-----------------------------------------------------------
		if (isset($options['tooltip'])) {
			
			// Jquery scripts
			$this->_View->startIfEmpty('tooltipJS');
				echo $this->Js->buffer(" $('*[data-toggle = \"tooltip\"]').tooltip(); ");
			$this->_View->end();
			
			// Default tooltip placement
			if(!isset($options['tooltip-position'])) {
				$options['tooltip-position'] = 'auto right';
			}
			
			// change the label to an array
			if (isset($options['label']) && !is_array($options['label'])) {
				$label = $options['label'];
				
				if ($label != '' && $label != false) {
					$options['label'] = array();
					$options['label']['text'] = $label;
				}
			}
			
			// Add the tooltip icon at the end of the label
			$options['label']['text'] .= ' <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="' . $options['tooltip-position'] . '" title="' . $options['tooltip'] . '"></i>';
			$options['escape'] = false;
			
			unset($options['tooltip']);
			unset($options['tooltip-position']);
		}
		
		//-----------------------------------------------------------HELP-----------------------------------------------------------
		if(isset($options['help'])) {
			if(!isset($options['after']))
				$options['after'] = '';
			$options['after'] .= '<span class="help-block">' . $options['help'] . '</span>';
			unset($options['help']);
		}
		
		//-----------------------------------------------------------ADDON-----------------------------------------------------------
		if(isset($options['addon'])) {
			
			if(is_array($options['addon'])) {
				$options['between'] = '<div class="input-group"><span class="input-group-addon">' . $options['addon'][0] . '</span>';
				$options['after'] = '<span class="input-group-addon">' . $options['addon'][1] . '</span></div>';
			}
			else {

				// Default addon placement
				if(!isset($options['addon-position'])) {
					$options['addon-position'] = 'right';
				}

				// If addon should be on the right
				if($options['addon-position'] == 'right') {
					$options['between'] = '<div class="input-group">';
					$options['after'] = '<span class="input-group-addon">' . $options['addon'] . '</span></div>';
				}

				// If addon should be on the left
				elseif($options['addon-position'] == 'left') {
					$options['between'] = '<div class="input-group"><span class="input-group-addon">' . $options['addon'] . '</span>';
					$options['after'] = '</div>';
				}

				// If you invented a position...
				else {
					throw new Exception("Addon position doesn't exist");
				}
			}
			
			unset($options['addon']);
			unset($options['addon-position']);
		}
		
		//-----------------------------------------------------------BUTTON-----------------------------------------------------------
		if(isset($options['button'])) {

			if(!isset($options['button-position'])) {
				$options['button-position'] = 'right';
			}
			if(!isset($options['button-class'])) {
				$options['button-class'] = 'btn-default';
			}

			// If addon should be on the right
			if($options['button-position'] == 'right') {
				$options['between'] = '<div class="input-group">';
				$options['after'] = '<span class="input-group-btn"><button class="btn '.$options['button-class'].'" type="button">' . $options['button'] . '</button></span></div>';
			}

			// If addon should be on the left
			elseif($options['button-position'] == 'left') {
				$options['between'] = '<div class="input-group"><span class="input-group-btn"><button class="btn '.$options['button-class'].'" type="button">' . $options['button'] . '</button></span>';
				$options['after'] = '</div>';
			}

			// If you invented a position...
			else {
				throw new Exception("Button position doesn't exist");
			}

			unset($options['button']);
			unset($options['button-position']);
			unset($options['button-class']);
		}

		
		//-----------------------------------------------------------PER-TYPE-----------------------------------------------------------
		if (isset($options['options']))
			$options['type'] = 'select';

		if(isset($options['type'])) {

			// Gives a default height to the textareas
			if ($options['type'] == 'textarea') {

				if(!isset($options['rows']))
					$options['rows'] = '3';

				$options['cols'] = false;
			}
			elseif($options['type'] == 'select') {
				
				if(!isset($options['between']))
					$options['between'] = '';
				if(!isset($options['after']))
					$options['after'] = '';
				
				$options['between'] .= '<div class="select-wrapper">';
				$options['after'] .= '</div>';
			}
			elseif($options['type'] == 'checkbox') {
				
			}
			elseif($options['type'] == 'radio') {

				if(isset($options['inline'])) {
					$options['between'] = '<div class="radio inline">';
					$options['separator'] = '</div><div class="radio inline">';
					unset($options['inline']);
				}
				else {
					$options['between'] = '<div class="radio">';
					$options['separator'] = '</div><div class="radio">';
				}
			}
		}
		
		return parent::input($fieldName, $options);
	}
}