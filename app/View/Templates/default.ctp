<?php echo $this->Html->css('main', array('inline' => false)); ?>

<?php

  $socialmedia = array(
    'facebook' => array('url' => 'https://www.facebook.com/RyersonRC4', 'icon' => 'fa-facebook'),
    'linkedin' => array('url' => 'http://www.linkedin.com/company/3585549?trk=tyah&trkInfo=tarId%3A1395413233452%2Ctas%3Aryerson%20cloud%2Cidx%3A1-1-1', 'icon' => 'fa-linkedin'),
    'twitter' => array('url' => 'https://twitter.com/RyersonRC4', 'icon' => 'fa-twitter'),
    'youtube' => array('url' => 'https://www.youtube.com/channel/UCmRN-A1KKtO3PGsoA7sMwHQ/feed', 'icon' => 'fa-youtube')
  );
  
  $our_nodes = array(
    'Our Nodes' => array(
      array('title' => 'Centre for Cloud and Context Aware Computing', 'url' => 'http://rc4.ryerson.ca'),
      array('title' => 'Transmedia Centre', 'url' => 'http://rtatransmediacentre.com/'),
      array('title' => 'Centre for Cloud Based Manufacturing', 'url' => '#')
    )
  );
  
  $contact = array(
    'Contact Us' => array(
      array('title' => '10 Dundas St East, Suite 1000', 'span' => ''),
      array('title' => 'Toronto, Canada M5B 2K3', 'span' => ''),
      array('title' => '416-979-5000 ext. 4921', 'span' => 'Telephone: '),
      array('title' => 'rc4@ryerson.ca', 'span' => 'Email: '),
    )
  );
  
  $footer_lists = array(
    'Privacy Policy' => 'http://www.ryerson.ca/privacy/',
    'Accessibility' => 'http://www.ryerson.ca/accessibility/',
    'Terms & Conditions' => 'http://www.ryerson.ca/ryerson.ca/terms.html'
  );

?>

<div id="header">
  <div id="header-top">
    <div class="container">
      <div class="row">
        
        <div class="col-sm-12">
          <?php
            echo $this->Html->link(
              $this->Html->image("RC4-logo.png", array("alt" => "RC4 Logo", 'id' => 'logo')),
              "/", array('escape' => false, 'inline' => false));?>
        </div>
        
        <div class="col-lg-offset-6 col-lg-6 col-sm-offset-4 col-sm-8 text-right">
          <div class="block">
            <?php echo $this->Html->link('&gt; Login', '/',
              array('class' => 'btn btn-warning', 'target' => '_blank', 'escape' => false)); ?>
          </div>
          <div class="block">
            <?php
              echo $this->Html->link(
                $this->Html->image("ryerson-logo.jpg", array("alt" => "Ryerson University Logo")),
                "http://ryerson.ca/",
                array('escape' => false, 'target' => 'blank', 'inline' => false)
              );           
            ?>
          </div>
          
        </div>
      </div>
    </div>
  </div>
    
  <nav class="navbar-default" role="navigation">
    <div class="container">
      
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
      <div class="row">
        <div class="col-xs-24">
        
          <ul class="nav navbar-nav navbar-left" id="bs-example-navbar-collapse-1">
            <?php
            
              if (!isset($nav_selected)) {
                $nav_selected = '';
              }    
              foreach($nav as $label => $link): 
                if ($nav_selected == $label) {
                  $options = array(
                    'class' => 'selected'
                  );
                } else {
                  $options = array();
                }
            ?>
            
            <li><?php echo $this->Html->link($label, $link, $options);?></li>    
            <?php endforeach; ?>
          </ul>
          
          <ul class="nav navbar-nav navbar-right">
            <?php foreach ($socialmedia as $name => $info): ?>
            <li>
              <a class="socialmedia" href="<?php echo $info['url']; ?>" target="_blank" title="<?php echo $name; ?>">
                <i class="fa <?php echo $info['icon']; ?>"></i>
              </a>
          </li>
          <?php endforeach; ?>
          </ul>
          
        </div>
      </div>
    </div>
  </nav>
    
  <div class="well">
    <div class="container">
      <div class="row">
        <div class="col-xs-24">
          <h1><?php echo $title ?></h1>
        </div>
      </div>
    </div>
  </div>
    
    
</div>

<div class="container content-wrapper">
  <?php echo $this->fetch('content'); ?>
</div>

<div id="preFooter">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php
          echo $this->Html->link(
            $this->Html->image("RC4-logo.png", array("alt" => "RC4 Logo")),
            "http://rc4.ryerson.ca",
            array('escape' => false, 'target' => 'blank', 'inline' => false)
          );           
        ?>
      </div> 
      <div class="col-sm-6">
        <?php echo $this->Html->link('&gt; Be Part of RC4!', '/', array('class' => 'btn btn-warning', 'escape' => false)); ?>
      </div>
      <div class="col-sm-6">
        <?php echo $this->Html->link('&gt; myryerson.ca', 'https://my.ryerson.ca/', array('class' => 'btn btn-warning', 'target' => '_blank', 'escape' => false)); ?>
      </div>
    </div>
    <div id="footer-content" class="row">
      <div class="col-sm-10">
        <p>
          RC4 is helping fuel productivity and growth in companies across vertical markets including; Manufacturing, Consumer (Retail, Entertainment, Media), Transportation, Health Informatics, and Public Safety. RC4 does this by providing the critical mass of infrastructure, experts, and collaboration necessary to stay competitive. Please contact RC4 or one of our nodes to learn more about how we can support your R&D strategy.
        </p>
      </div>
      <div class="col-sm-offset-2 col-sm-6">
        <h3 class="margin-top-0">Our Nodes</h3>
        <ul class="list-unstyled">
          <li><p>Centre for Cloud and Context Aware Computing</p></li>
          <li><p>Transmedia Centre</p></li>
          <li><p>Centre for Cloud Based Manufacturing</p></li>
        </ul>
      </div>
      <div class="col-sm-6">
        <h3 class="margin-top-0">Contact Us</h3>
        <ul class="list-unstyled">
          <li><p>10 Dundas St East, Suite 1000</p></li>
          <li><p>Toronto, Canada M5B 2K3</p></li>
          <li><p>Telephone: 416-979-5000 ext. 4921</p></li>
          <li><p>Email: <?php echo $this->Text->autoLinkEmails('rc4@ryerson.ca'); ?></p></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div id="footer">
  <div class="container">
    <div id="footer-logo">
      
      <div class="col-sm-offset-5 col-sm-8">
        <?php echo $this->Html->image('federal-economic-department-logo.jpg', array('alt' => 'Federal Economic Department logo')); ?>
      </div>
      
      <div class="col-sm-6">
        <?php echo $this->Html->image('federal-economic-department-logo-2.jpg', array('alt' => 'Federal Economic Department logo')); ?>
      </div>
      
    </div>
  </div>
</div>

<div id="copy-right">
  <div class="container text-center">
    <p>&copy <?php echo Date('Y'); ?> Ryerson University | 350 Victoria Street, Toronto, Ontario, Canada M5B 2K3</p>
    
    <ul class="list-unstyled list-inline">
      <?php foreach($footer_lists as $name => $link):?>
      <li><?php echo $this->Html->link($name, $link, array('target' => 'blank', 'inline' => false)); ?></li>
      <?php endforeach; ?>
    </ul>
    
  </div>
</div>








