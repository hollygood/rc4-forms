<?php
	echo $this->extend('/Templates/default');
	
	echo $this->Html->script('jquery-ui-1.10.4.custom.min', array('inline' => false));
	echo $this->Html->css('smoothness/jquery-ui-1.10.4.custom.min', array('inline' => false));
		

	$this->Js->get('#sortable tbody')->sortable(array(
		'items' => 'tr.table-content',
		'handle' => '> td > .fa-bars',
		'revert' => true,
		'placeholder' => 'ui-state-highlight',
		'cursor' => 'move',
 	));

	
	$this->Js->buffer('
		giveWidthToTds();
		
		function giveWidthToTds() {
			$("#sortable td").css("width", "auto");
			
			$("#sortable td").each(function() {
				$(this).css("width", $(this).width());
			});			
		}		
	');
	
	$this->Js->get('window')->event('resize', 'giveWidthToTds();', true);	
?>

<div class="row">
	
	<div class="col-xs-24">
		
		<?php echo $this->Session->flash(); ?>
		
		<div class="text-right add">
			<?php
				echo $this->Html->link(
					'Add Form Field',
					array('controller' => 'custom_form_fields', 'action' => 'add', $custom_form_id),
					array('class' => 'btn btn-danger')
				);
			?>
		</div>
		
		<table id="sortable" class="table table-bordered table-striped">
		
			<?php
				
				echo $this->Html->tableHeaders( array(
					array('' => array('style' => 'width: 1%')),
					'Question', 'Type', 'Options',
					array('Edit' => array('class' => 'narrow')),
					array('Delete' => array('class' => 'narrow')),
				));
				
				$rows = array();
				
				foreach ($customformfields as $customformfield) {
					
					$rows[] = array(
						'<i class="fa fa-bars text-muted"></i>',
						$customformfield['CustomFormField']['name'],
						$customformfield['CustomFormField']['type_label'],
						$customformfield['CustomFormField']['options'],
						
						array(
							$this->Html->link('<i class="fa fa-pencil-square-o fa-lg"></i>',
								array('action' => 'edit', $customformfield['CustomFormField']['id']),
								array('escape' => false, 'title' => 'Edit')
							),
							array('class' => 'narrow')
						),
						
						array(
							$this->Form->postlink('<i class="fa fa-trash-o fa-lg"></i>', array('action' => 'delete', $customformfield['CustomFormField']['id']),
								array(
									'confirm' => 'Are you sure to delete this question?',
									'escape' => false, 'title' => 'Delete'
								)
							),
							array('class' => 'narrow')
						),
	
					);
							
				}
				
				echo $this->Html->tableCells($rows, array('class' => 'table-content'), array('class' => 'table-content'));
			
			?>
		
		</table>
	</div>
	
</div>

