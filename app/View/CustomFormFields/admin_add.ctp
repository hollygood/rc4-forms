<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
	<div class="col-xs-24">
		<?php
			echo $this->Session->flash();
			echo $this->Form->create();
			echo $this->Form->input('name', array('label' => __('Question')));
			echo $this->Form->input('type', array('options' => $types, 'default' => 'checkbox'));
			echo $this->Form->input('options', array('label' => __('Options <small>(Only for dropdowns, radio buttons or multiple checkboxes)</small><br /> <span class="text-danger">*Please separate them with line breaks</span>')));
			echo $this->Form->submit(__('Add'));
			echo $this->Form->end();
		?>
	</div>
</div>