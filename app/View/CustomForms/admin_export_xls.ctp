<?php
$this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);

//------------------------------------------------------------------

$headers = array(
	array('label' => 'First Name', 'filter' => true, 'wrap' => true, 'width' => '20'),
	array('label' => 'Last Name', 'filter' => true, 'wrap' => true, 'width' => '20'),
	array('label' => 'Email', 'filter' => true, 'wrap' => true, 'width' => '40'),
);

foreach ($user_form_fields as $user_form_field) {
	$headers[] = array('label' => $user_form_field['CustomFormField']['name'], 'filter' => true, 'wrap' => true, 'width' => '50');
}	 


$this->PhpExcel->addTableHeader($headers, array('bold' => true, 'background' => '666666', 'color' => 'FFFFFF'));

//------------------------------------------------------------------

$rows = array();
foreach ($user_forms as $user_form) {
	$row = array(
		$user_form['User']['first_name'],
		$user_form['User']['last_name'],
		$user_form['User']['email']
	);
	
	foreach($user_form['UserFormValue'] as $form_value) {
		if(is_array($form_value['value']))
			$row[] = implode(', ', $form_value['value']);
		else
			$row[] = $form_value['value'];
	}
	
	$this->PhpExcel->addTableRow($row);
}


//------------------------------------------------------------------

$this->PhpExcel->addTableFooter()->output();