<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
  <div class="col-xs-24">
    
		<?php echo $this->Session->flash(); ?>
		
		<table class="table table-bordered table-striped">
		
		<?php
				
			echo $this->Html->tableHeaders( array(
				'Form Name', 'View', 'Edit' 
			));
			
			$rows = array();

				
				foreach ($forms as $customform) {
				
				$row = array(
					
					$customform['CustomForm']['name'],
					
					$this->Html->link('<i class="fa fa-eye fa-lg"></i>',
						array('action' => 'view', $customform['CustomForm']['id']),
						array('escape' => false, 'title' => 'Edit')
					),
					
					$this->Html->link('<i class="fa fa-pencil-square-o fa-lg"></i>',
						array('action' => 'edit', $customform['CustomForm']['id']),
						array('escape' => false, 'title' => 'Edit')
					)
					
						
				);
				$rows[] = $row;
				
				
				}
				
				echo $this->Html->tableCells($rows);
			
		?>
		
		</table>
		
  </div>
</div>