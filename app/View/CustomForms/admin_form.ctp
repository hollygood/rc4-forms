<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">

	<div class="col-xs-24">
		<?php
			echo $this->Session->flash();
			echo $this->Form->create(); 
			echo $this->Form->input('name');
			
			echo $this->Form->input('status', array(
					'options' => $status,
					'empty' => 'Select a status'));
			
			echo $this->Form->submit(__('Save'));
			echo $this->Form->end();
		?>
	</div> 
	
	
</div>