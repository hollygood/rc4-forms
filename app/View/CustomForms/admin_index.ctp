<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
	
	<div class="col-xs-24">
		
		<?php echo $this->Session->flash(); ?>
		
		<div class="text-right add">
			<?php
				echo $this->Html->link(
					'Add Form',
					array('controller' => 'custom_forms', 'action' => 'add'),
					array('class' => 'btn btn-danger')
				);
			?>
		</div>
		
		
		<table id="sortable" class="table table-striped table-bordered">
		
			<?php
				
				echo $this->Html->tableHeaders(
					array(
						array('Status' => array('class' => 'narrow')),
						array('Form Name' => array('style' => 'width: 30%')),
						array('Total Entries' => array('style' => 'width: 20%')),
						array('Last Entry' => array('style' => 'width: 20%;')),
						array('Entries' => array('class' => 'narrow')),
						array('Fields' => array('class' => 'narrow')),
						array('Delete' => array('class' => 'narrow')),
					)
				);
				
				$rows = array();
				
				foreach ($customforms as $customform) {
					
					$rows[] = array(
						$customform['CustomForm']['status'],
						
						$customform['CustomForm']['name'] . ' ' . $this->Html->link('(Edit)',
							array('action' => 'edit', $customform['CustomForm']['id']),
							array('escape' => false, 'title' => 'Edit Form')
						),
						
						array(
							$customform['CustomForm']['user_forms_count'],
							array('class' => 'bigger-text')
						),
						
						$this->Time->timeAgoInWords($customform['CustomForm']['last_modified']),
						
						array(
							$this->Html->link('<i class="fa fa-eye fa-lg"></i>',
								array('action' => 'admin_view', $customform['CustomForm']['id']),
								array('escape' => false, 'title' => 'View all entries')
							),
							array('class' => 'narrow')
						),
						
						array(
							$this->Html->link('<i class="fa fa-pencil-square-o fa-lg"></i>',
								array('controller' => 'custom_form_fields', 'action' => 'index',  $customform['CustomForm']['id']),
								array('escape' => false, 'title' => 'Edit form fields')
							),
							array('class' => 'narrow')
						),
						
						array(
							$this->Form->postLink('<i class="fa fa-trash-o fa-lg"></i>',
								array('action' => 'delete', $customform['CustomForm']['id']),
								array('escape' => false, 'title' => 'Delete form', 'confirm' => 'Are you sure?')
							),
							array('class' => 'narrow')
						),
						
					);
				}
				
				echo $this->Html->tableCells($rows, array('class' => 'table-content'), array('class' => 'table-content'));
			
			?>
		
		</table>	
		
	</div>	
</div>


