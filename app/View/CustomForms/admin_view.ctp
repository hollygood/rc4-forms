<?php
	echo $this->extend('/Templates/default');
	echo $this->Html->script('jquery.shorten', array('inline' => false));
	echo $this->Js->buffer('
		$(".more").shorten({
			moreText: "[View&nbsp;Full&nbsp;Answer]",
			lessText: "[Show&nbsp;Less]",
			//showChar: "150",
			ellipsesText: "...",
		});
	');
?>

<div class="row">
	<div class="col-xs-24">
  
		<?php echo $this->Session->flash(); ?>
		
		<div class="text-right add">
		<?php echo $this->Html->link(__('Save to Excel document'), array('controller' => 'custom_forms', 'action' => 'export_xls', 'admin' => true, $this->params->pass[0]), array('class' => 'btn btn-danger')); ?>
		</div>
		
		<table class="table table-bordered table-striped">
		
		<?php
			$headers = array(
				'First Name', 'Last Name', 'Email'
			);
	 
			foreach ($user_form_fields as $user_form_field) {
				$headers[] = $user_form_field['CustomFormField']['name'];
			}	 
	 
			echo $this->Html->tableHeaders($headers);
		
			$rows = array();
			
		  foreach ($user_forms as $user_form) {

				$row = array(
					$user_form['User']['first_name'],
					$user_form['User']['last_name'],
					$user_form['User']['email']
				);
				
				foreach($user_form['UserFormValue'] as $form_value) {
					if(is_array($form_value['value']))
						$row[] = implode(', ', $form_value['value']);
					else
						$row[] = array($form_value['value'], array('class' => 'more'));
				}
				
				$rows[] = $row;
				
			}
				
			echo $this->Html->tableCells($rows);
		?>
		
		</table>
  
  </div>
</div>