<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
  <div class="col-xs-24">
    <?php
      echo $this->Session->flash(); 
      echo $this->Form->create();
			echo $this->Form->input('password');
			echo $this->Form->input('password2', array(
				'label' => 'Re-type Password',
				'type' => 'password'
			));

			echo $this->Form->input('id');
			echo $this->Form->submit(__('Update'));
    ?>

    <?php echo $this->Form->end(); ?>
  </div>
</div>
