<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
  <div class="col-xs-24">
    <?php
      echo $this->Form->create();
      echo $this->Session->flash();
      echo $this->Form->input('first_name');
      echo $this->Form->input('last_name');
      echo $this->Form->input('email');
      echo $this->Form->input('password');
      echo $this->Form->input('password2', array(
        'label' => 'Re-enter the Password',
        'type' => 'password'
      ));
      echo $this->Form->input('role', array(
        'type' => 'select',
        'options' => 
          array('Admin' => 'A', 'User' => 'U'),
          'empty' => 'Select Role'
      ));
      
      echo $this->Form->submit(__('Save'));
      
      echo $this->Form->end();
    ?>
  </div>
</div>
