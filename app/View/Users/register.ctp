<?php
	echo $this->extend('/Templates/default');
?>

<div class="users form">
	<?php
		echo $this->Form->create();

		echo $this->Form->input('first_name'); 
		echo $this->Form->input('last_name');
		echo $this->Form->input('email'); 
		echo $this->Form->input('password'); 
		echo $this->Form->input('password2', array('label' => 'Re-enter the Password', 'type' => 'password'));
		echo $this->Form->submit(__('Submit'));
		echo $this->Form->end();
	?>
</div>