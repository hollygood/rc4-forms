<?php
	echo $this->extend('/Templates/default');
?>

<div class="container">
	<div class="row">
		
		<div class="col-sm-12">
			<div class="users form">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->Form->create(); ?>
				<fieldset>
					<legend><?php echo __('Please enter your email and password') ?></legend>
					<?php echo $this->Form->input('email') ?>
					<?php echo $this->Form->input('password') ?>
				</fieldset>
				<?php echo $this->Form->end(__('Login')); ?>	
			</div>
		</div>
		
		<div class="col-sm-12 add">
			<fieldset>
				<legend><?php echo __('New Users?') ?></legend>
			</fieldset>
			<?php echo $this->Html->link('Click here to Register',
				array('controller' => 'users', 'action' => 'register'),
				array('escape' => false, 'class' => 'btn btn-danger'));
			?>
		</div>
		
		</div>
	</div>
</div>