<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
	
	<div class="col-xs-24">
		
		<?php 
			echo $this->Session->flash(); 
		?>
		<div class="text-right add">
			<?php echo $this->Html->link('Add User', array(
					'controller' => 'users',
					'action' => 'add',
					'admin' => true
					),
					array('class' => 'btn btn-danger')
				);
			?>
		</div>  
		
		<table class="table table-bordered table-striped">
			
			<?php
				
				echo $this->Html->tableHeaders( array(
					'First Name', 'Last Name', 'Email', 'Role', 'Password', 'Edit', 'Delete'
				));
			
				$rows = array();
				
				foreach ( $users as $user ) {
					$row = array(
						
						$user['User']['first_name'],
						$user['User']['last_name'],
						$user['User']['email'],
						$user['User']['role'],
						
						$this->Html->link('Password',
							array('action' => 'password', $user['User']['id'], 'admin' => true)
						),
						
						$this->Html->link('<i class="fa fa-pencil-square-o fa-lg"></i>',
								array('action' => 'edit', $user['User']['id']),
								array('escape' => false, 'title' => 'Edit')
							),
						
						$this->Form->postlink('<i class="fa fa-trash-o fa-lg"></i>', array('action' => 'delete', $user['User']['id']),
							array(
								'confirm' => 'Are you sure to delete this user?',
								'escape' => false, 'title' => 'Delete'
							)
						)
						
					);
					
					$rows[] = $row;
				}
				
				echo $this->Html->tableCells($rows);
				
			?>
			
			
		</table>
		
	</div>
</div>
