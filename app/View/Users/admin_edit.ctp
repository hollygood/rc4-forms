<?php
	echo $this->extend('/Templates/default');
?>


<div class="row">
  <div class="col-xs-24">
    <?php
      echo $this->Form->create();
      echo $this->Session->flash();
      echo $this->Form->input('first_name');
      echo $this->Form->input('last_name');
      echo $this->Form->input('email');
      echo $this->Form->input('role', array(
        'type' => 'select',
        'options' => 
          array('A' => 'Admin', 'U' => 'User')
      ));
      
      echo $this->Form->submit(__('Save'));
      
      echo $this->Form->end();
    ?>
  </div>
</div>
