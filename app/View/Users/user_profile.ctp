<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
  <div class="col-sm-24">
    
		<?php
			echo $this->Session->flash();
			echo $this->Form->create();
			echo $this->Form->input('first_name');
			echo $this->Form->input('last_name');
			echo $this->Form->input('email');
			echo $this->Form->submit('Update');
			echo $this->Form->end();
		?>

  </div>
</div>