<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<!--[if lt IE 9]><html class="no-js lt-ie-9"><![endif]-->
<!--[if gte IE 9]><html><![endif]-->
<head>
	<?php 
		echo $this->Html->charset(); 
	?>
	<title>RC4 FORMS<?php if($title_for_layout !== false) echo " &bull; " . $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo '<meta name="viewport" content="width=device-width, initial-scale=1">';	
		echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

		echo $this->fetch('css');
		echo $this->Html->css('smoothness/jquery-ui-1.10.4.custom.min');
	?>

	<!--[if lt IE 9]>
		<link href="/css/ie-fixes.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="/js/ie-polyfills/html5shiv-printshiv.js"></script>
		<script type="text/javascript" src="/js/ie-polyfills/respond.min.js"></script>
	<![endif]-->
	
</head>
<body>
	<?php 
		// echo $this->element('sql_dump'); 
	?>
	
	<?php echo $this->fetch('content'); ?>

	
	<?php
		echo $this->Html->script('jquery-1.11.0.min');
		
		//echo $this->Html->script('bootstrap/affix');
		//echo $this->Html->script('bootstrap/alert');
		//echo $this->Html->script('bootstrap/button');
		echo $this->Html->script('bootstrap/collapse');
		echo $this->Html->script('bootstrap/dropdown');
		//echo $this->Html->script('bootstrap/tab');
		echo $this->Html->script('bootstrap/transition');
		//echo $this->Html->script('bootstrap/scrollspy');
		//echo $this->Html->script('bootstrap/modal');
		//echo $this->Html->script('bootstrap/popover');
		
		echo $this->Html->script('jquery-ui-1.10.4.custom.min');

		echo $this->fetch('script');
		echo $this->Js->writeBuffer();
	?>
</body>
</html>