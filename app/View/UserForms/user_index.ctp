<?php
	echo $this->extend('/Templates/default');
?>

<div class="row">
  <div class="col-xs-24">
    
    <?php echo $this->Session->flash(); ?>
    
    <?php
			echo $this->Form->create();
    
      echo $this->Form->input('custom_form_id', array('type' => 'select',
				'options' => $custom_forms,
        'empty' => 'Select Form')); 
    
    ?>
    
    <?php
      echo $this->Form->submit(__('Submit'));
      echo $this->Form->end();
			
		?>
		
		<table class="table table-bordered table-striped">
		
		<?php 
	
			echo $this->Html->tableHeaders( array(
				'Form Name', 'Date Created', 'Edit', 'Delete'
			));
			
			$rows = array();
			
			foreach ($user_forms as $user_form ) {
					
				$rows[] = array(
				
					$user_form['CustomForm']['name'],
					$user_form['UserForm']['created'],
					
					$this->Html->link('<i class="fa fa-pencil-square-o fa-lg"></i>',
						array('action' => 'edit', $user_form['UserForm']['id']),
						array('escape' => false, 'title' => 'Edit')
					),
					
					$this->Form->postlink('<i class="fa fa-trash-o fa-lg"></i>', array('action' => 'delete', $user_form['UserForm']['id']),
						array(
							'confirm' => 'Are you sure to delete this user?',
							'escape' => false, 'title' => 'Delete'
						)
					)
					
				);

			}
			
			echo $this->Html->tableCells($rows);
			
    ?>
		
		</table>
    
  </div>
</div>