<?php
	echo $this->extend('/Templates/default');
?>

<?php

  echo $this->Form->create();

  foreach( $user_form['CustomForm']['CustomFormField'] as $user_custom_form ) {

    $id = $user_custom_form['id'];
		
		if($user_custom_form['type'] == 'radio') {
			echo $this->Form->radio("UserFormValue.$id.value", $user_custom_form['options_array'], array('legend' => $user_custom_form['name'], 'hiddenField' => false));
		}
		else {
			$options = array(
				'label' => $user_custom_form['name'],
				'type' => $user_custom_form['type']
			);
			
			if (isset($user_custom_form['options_array'])) {
				$options['options'] = $user_custom_form['options_array'];
			}
			
			if($user_custom_form['type'] == 'checkbox') {
				$options['value'] = 'Yes';
				$options['hiddenField'] = 'No';
			}
			elseif($user_custom_form['type'] == 'checkboxes') {
				$options['multiple'] = 'checkbox';
				$options['hiddenField'] = 'None';
			}
			elseif($user_custom_form['type'] == 'select') {
				$options['empty'] = '--';
			}
			
			echo $this->Form->input("UserFormValue.$id.value", $options);
		}
		echo $this->Form->input("UserFormValue.$id.id");
	}
	
	
	
	echo $this->Form->input("id");
  
  echo $this->Form->submit(__('Save'));
  echo $this->Form->end();
 