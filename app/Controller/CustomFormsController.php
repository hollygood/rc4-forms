<?php

App::uses('AppController', 'Controller');

class CustomFormsController extends AppController {
	
	public function admin_index() {
		$customforms = $this->CustomForm->find('all');
		$title = 'Dashboard';
		$nav_selected = "Dashboard";
		$this->set(compact('customforms', 'title', 'nav_selected'));	
	}
	
	public function admin_add() {
		$title = 'Add Form';
		$this->set(compact('title', 'nav_selected'));
		$this->_admin_form();
	}
	
	public function admin_edit($id = null) {
		
		$title = 'Edit Form';
		$this->set(compact('title', 'nav_selected'));	
		
		if(!$this->CustomForm->exists($id)) {
			throw new NotFoundException(__('Invalid Form'));
		}
		
		$this->_admin_form($id);
	}
	
	private function _admin_form($id = null) {
		
		$status = $this->CustomForm->status();
		
		$this->set(compact('status'));
		
		$this->CustomForm->id = $id;
		
		if($this->request->is(array('post', 'put'))) { 
			if($this->CustomForm->id == null) {
				$this->CustomForm->create();
			}
			
			if($this->CustomForm->save($this->request->data)) {
				$this->Session->setFlash(__('The form has been saved'), 'flash_success');
				return $this->redirect(array('controller' => 'custom_forms', 'action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__('The form could not be saved. Please try again.'), 'flash_danger'); 
			} 
		}
		
		else {
			$this->request->data = $this->CustomForm->read();
		}
		
		$this->render('admin_form');
	}
	
	public function admin_delete($id = null) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		
		if ($this->CustomForm->delete($id)) {
			$this->Session->setFlash(__('The custom form with id %s has been deleted.', h($id)), 'flash_success');
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function admin_view($id = null) {
		
		$title = 'View';
		$nav_selected = "Form";
		
		if (!$id) {
			throw new NotFoundException (__('Invalid custom form'));
		}
		
		$user_forms = $this->CustomForm->UserForm->find('all', array(
			'contain' => array(
				'CustomForm',
				'UserFormValue',
				'User'
			),
			'conditions' => array(
				'UserForm.custom_form_id' => $id
			)
		));
		
		$user_form_fields = $this->CustomForm->CustomFormField->find('all', array(
			'contain' => array('CustomForm'),
			'conditions' => array(
				'CustomFormField.custom_form_id' => $id
			)
		));
		
		$this->set(compact('title', 'nav_selected', 'user_forms', 'user_form_fields'));
	}
	
	public function admin_export_xls($id) {
		
		if (!$this->CustomForm->exists($id))
			throw new NotFoundException (__('Invalid custom form'));
		
		$user_forms = $this->CustomForm->UserForm->find('all', array(
			'contain' => array(
				'CustomForm',
				'UserFormValue',
				'User'
			),
			'conditions' => array(
				'UserForm.custom_form_id' => $id
			)
		));
		
		$user_form_fields = $this->CustomForm->CustomFormField->find('all', array(
			'contain' => array('CustomForm'),
			'conditions' => array(
				'CustomFormField.custom_form_id' => $id
			)
		));
		
		$this->set(compact('user_forms', 'user_form_fields'));
	}
	
	//------------------------------ User ------------------------------------
	
	public function user_index() {
		$title = 'Dashboard';
		$forms = $this->CustomForm->find('all');
		
		$this->set(compact('title', 'forms'));
	}
	
	public function user_view( $id = null) {
		$this->set('title', 'View Form');
		$this->CustomForm->id = $id;
	}

}
	
?>