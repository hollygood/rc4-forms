<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('dashboard', 'register');
	}
	
	public function register() {
		
		$this->set('title', 'Register');
		
		if ($this->request->is('Post')) {
			
			$this->request->data['User']['role'] = 'U';

			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Thank you for registering!'));
				$this->Auth->login($this->request->data['User']['email']);
				
				return $this->redirect(array('action' => 'index', 'user' => true));
			}
			$this->Session->setFlash(__('The user could not be saved'));
		}
	}
	
	public function login() {
		$this->set('title', 'Login');
		
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Auth->login()) {
	
				if ($this->Auth->user('role') == 'A') {
					return $this->redirect(array('controller' => 'custom_forms', 'action' => 'index', 'admin' => true, 'user' => false));
				}
				else {
					return $this->redirect(array("controller" => 'user_forms', 'action' => 'index', 'user' => true, 'admin' => false));
				}
			}
			
			$this->Session->setFlash(__('Invalid username or password'), 'flash_danger');
		}
	}
	
	public function logout() {
		return $this->redirect($this->Auth->logout());
	}
	
	//-------------------------------- Admin -------------------------------------
	
	public function admin_index() {
		$users = $this->paginate();
		$title = 'Users';
		$nav_selected = "Users";
		$this->set(compact('users', 'title', 'nav_selected'));
	}
	
	public function admin_add() {
		
		$title = 'Add User';
		$nav_selected = "Users";
		$this->set(compact('title', 'nav_selected'));
		
		if ($this->request->is(array('post', 'put'))) {
			
			$this->User->create();
			
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'flash_success');
				$this->redirect( array('action' => 'admin_index', 'admin' => true));
			}
			
			$this->Session->setFlash(__('The user can not be saved, try again.'), 'flash_danger');
		}
	}
	
	public function admin_edit($id = null) {
		
		$title = 'Edit User';
		$nav_selected = "Users";
		$this->set(compact('title', 'nav_selected'));
    
		$this->User->id = $id;
		
		if(!$this->User->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->redirect( array('action' => 'admin_index', 'admin' => true));
			}
			
			$this->Session->setFlash(__('The user can not be saved, try again.'), 'flash_danger');
		}
		
		else {
			$this->request->data = $this->User->read();
			unset($this->request->data['User']['password']);
		}
	}
	
	public function admin_delete($id = null) {
		$this->request->onlyAllow('post');
		$this->User->id = $id;
		
		if(!$this->User->exists()) {
      throw new NotFoundException(__('Invalid User'));
    }
		
		if($this->User->delete($id)) {
			$this->Session->setFlash(__('User Deleted'), 'flash_success');  
		}
		
		else {
      $this->Session->setFlash(__('User was not deleted'), 'flash_danger');
    }    
    
    $this->redirect(array('action' => 'admin_index', 'admin' => true));
		
	}
	
	public function admin_password($id) {
		
		$title = 'Reset Password';
		$nav_selected = "Users";
		$this->set(compact('title', 'nav_selected'));
		
    $this->User->id = $id;  
  
    if(!$this->User->exists()) {
      throw new NotFoundException(__('Invalid User'));
    }  
  
    if($this->request->is(array('post', 'put'))) {
      
			if($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Password has been saved.'), 'flash_success');
        return $this->redirect(array('action' => 'admin_index', 'admin' => true));
      }
			
      $this->Session->setFlash(__('Password cannot be saved, try again.'), 'flash_danger');
    }
		
    else {
      $this->request->data = $this->User->read();
      unset($this->request->data['User']['password']);
    }
		
  }
	
	//-------------------------------- User -----------------------------------------
	
	public function user_profile() {

		$user = $this->User->findById($this->Auth->user('id'));
		$title = 'Profile';
		$nav_selected = 'My Profile';
		$this->set(compact('user', 'title', 'nav_selected'));
		
		if($this->request->is(array('post', 'put'))) {
      
			$this->User->id = $this->Auth->user('id');
			
			if($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Profile has been saved'), 'flash_success');
				return $this->redirect(array('action' => 'user_profile'));
      }
			
      $this->Session->setFlash(__('Profile cannot be saved, try again.'), 'flash_danger');
    }
		
		if (empty($this->request->data)) {
			$this->request->data = $user;
		}
		
	}
	
	public function user_password() {
		$nav_selected = 'Update Password';
		$title = 'Update Password';
		$this->set(compact('nav_selected', 'title'));
	
    $this->User->id = $this->Auth->user('id');
		$this->request->data['User']['id'] = $this->User->id;
		
		if(!$this->User->exists()) {
      throw NotFoundException(__('Invalid User'));
    }
		
		if($this->request->is(array('post', 'put'))) {
			
			if($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Password has been updated.'), 'flash_success');
      }
			else {
				$this->Session->setFlash(__('Password cannot be saved, try again.'), 'flash_danger');
			}
    }
		
	}
	
}


?>