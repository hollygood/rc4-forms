<?php

App::uses('AppController', 'Controller');

class CustomFormFieldsController extends AppController {
	
	
	//-------------------------------- Admin -----------------------------------------
	
	public function admin_index($custom_form_id) {
		
		$title = 'Custom Form Fields';
		$nav_selected = "Form";
		$this->set(compact('title', 'nav_selected'));	

		$customformfields = $this->CustomFormField->find('all', array(
				'conditions' => array(
					'CustomFormField.custom_form_id' => $custom_form_id
				)
			));
		
		$this->set(compact('customformfields', 'custom_form_id'));
	}
	
	public function admin_add($custom_form_id) {
		
		$title = 'Add Custom Form Fields';
		$nav_selected = "Form";
		$this->set(compact('title', 'nav_selected'));	
		
		$types = $this->CustomFormField->types();
		$this->set('types', $types);

		if ($this->request->is('Post')) {
			
			$this->request->data['CustomFormField']['custom_form_id'] = $custom_form_id;

			$this->CustomFormField->create();
			if ($this->CustomFormField->save($this->request->data)) {
				$this->Session->setFlash(__('The custom form field has been saved'), 'flash_success');
				return $this->redirect(array('action' => 'index', $custom_form_id));
			}
			$this->Session->setFlash(__('The custom form field could not be saved'), 'flash_danger');
		}

	}
	
	public function admin_edit($id = null) {
		
		$title = 'Edit Custom Form Field';
		$nav_selected = "Form";
		$this->set(compact('title', 'nav_selected'));	
		
		$types = $this->CustomFormField->types();
		$this->set('types', $types);
		
		if (!$id) {
			throw new NotFoundException (__('Invalid custom form field'));
		}
		
		$data = $this->CustomFormField->findById($id);

		if (!$data) {
			throw new NotFoundException(__('Invalid custom form field'));
		}

		if ($this->request->is(array('post', 'put'))) {
			$this->CustomFormField->id = $id;
			
			if ($this->CustomFormField->save($this->request->data)) {
				$this->Session->setFlash(__('The custom form field has been updated'), 'flash_success');
				return $this->redirect(array('action' => 'index', $data['CustomFormField']['custom_form_id']));
			}
			$this->Session->setFlash(__('Unable to update this custom form field'), 'flash_danger');
		}
		
		if (!$this->request->data) {
			$this->request->data = $data;
		}
		
	}
	
	public function admin_delete($id = null) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		
		$data = $this->CustomFormField->findById($id);
		
		if ($this->CustomFormField->delete($id)) {
			$this->Session->setFlash(__('The custom form field with id %s has been deleted.', h($id)),'flash_success');
		}
		return $this->redirect(array('action' => 'index', $data['CustomFormField']['custom_form_id']));
	}
	
	//-------------------------------- User -----------------------------------------
	
	public function user_index() {
		$this->set('title', 'Form');
	}
	
	
}

?>