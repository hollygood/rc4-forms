<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */

class AppController extends Controller {
	
	public $helpers = array(
		'Html',
		'Form' => array('className' => 'BootstrapForm'),
		'Js',
		'Session',
		'PhpExcel.PhpExcel'
	);
	
	public $components = array(
		'Session',
		'Auth' => array(
			'loginAction' => array('controller' => 'users', 'action' => 'login', 'user' => false, 'admin' => false),
      'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'user' => false, 'admin' => false),
			'authorize' => array('Controller'),
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email')
				)
			)
		)
	);
	
	public function beforeFilter() {
		if( !isset($this->params['prefix']))
			$this->Auth->allow();
		
		$this->nav();
	}
	
	public function isAuthorized($user) {
		
		if (isset($this->params['prefix'])) {
			
			if ($this->params['prefix'] == 'admin') {
				if ($user['role'] === 'A')
					return true;
				else
					return false;
			}
			else {
				return true;
			}
		}
		
    return false;
	}
	
	public function nav() {
		
		$user = $this->Auth->user();
		
		if (empty($user))
			$nav = array();
		
		elseif ($this->Auth->user('role') == 'A') {
			$nav = array(
				//'Dashboard' => array( 'controller' => 'users', 'action' => 'admin_dashboard'),
				'Dashboard' => array( 'controller' => 'custom_forms', 'action' => 'index'),
				'Users' => array( 'controller' => 'users', 'action' => 'index'),
				'Logout' => array( 'controller' => 'users', 'action' => 'logout', 'admin' => false)
			);
		}
		
		else {
			$nav = array(
				'Dashboard' => array( 'controller' => 'user_forms', 'action' => 'index'),
				'My Profile' => array( 'controller' => 'users', 'action' => 'user_profile'),
				'Update Password' => array('controller' => 'users', 'action' => 'user_password'),
				'Logout' => array( 'controller' => 'users', 'action' => 'logout', 'user' => false)
			);
		}
		
		$this->set(compact("nav"));
		
	}
}