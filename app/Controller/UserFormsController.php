<?php

App::uses('AppController', 'Controller');

class UserFormsController extends AppController {
  
  public function user_index() {
    
    if($this->request->is(array('post', 'put'))) {
      $this->request->data['UserForm']['user_id'] = $this->Auth->user('id');
      
      $this->UserForm->create();
      
      if ($this->UserForm->save($this->request->data)) {
        
      }
      else {
        
      }
    }
    
    
    $custom_forms = $this->UserForm->CustomForm->find('list');
    $title = 'Dashboard';
    $nav_selected = 'Dashboard';
    
    $user_forms = $this->UserForm->find('all', array(
      'contain' => array('CustomForm'),
      'conditions' => array(
        'UserForm.user_id' => $this->Auth->user('id')
      )
    ));
    
    $this->set(compact('custom_forms', 'title', 'nav_selected', 'user_forms'));
  }
  
  public function user_edit($id) {
    
    $user_form = $this->UserForm->find('first', array(
      'contain' => array('CustomForm' => 'CustomFormField'),
      'conditions' => array(
        'UserForm.id' => $id
      )
    ));
    
    $title = 'Edit Form: ' . $user_form['CustomForm']['name'];
    
    $this->set(compact('user_form', 'title'));

    
    if($this->request->is(array('post', 'put'))) {

      foreach($this->request->data['UserFormValue'] as $key => $data) {
				
				
				
        $this->request->data['UserFormValue'][$key]['user_form_id'] = $id; 
        $this->request->data['UserFormValue'][$key]['custom_form_field_id'] = $key;
				
				
				if(is_array($this->request->data['UserFormValue'][$key]['value']))
					$this->request->data['UserFormValue'][$key]['value'] = json_encode($this->request->data['UserFormValue'][$key]['value']);
				
      }
			

      if($this->UserForm->UserFormValue->saveMany($this->request->data['UserFormValue'])){
        $this->Session->setFlash(__('The form has been saved'), 'flash_success');
				$this->redirect( array('action' => 'user_index', 'user' => true));
      }
    }
    else {
      
      $this->request->data['UserFormValue'] = array();

      $data = $this->UserForm->UserFormValue->find('all', array(
				'conditions' => array(
					'UserFormValue.user_form_id' => $id
				)   
			));

      foreach ($data as $values) {
        $custom_form_field_id = $values['UserFormValue']['custom_form_field_id'];
        $this->request->data['UserFormValue'][$custom_form_field_id] = $values['UserFormValue'];
      }
			
    }

    
  }
  
  public function user_delete($id = null) {
    $this->request->onlyAllow('post');
		$this->UserForm->id = $id;
    
    if($this->UserForm->delete()) {
			$this->Session->setFlash(__('User Form Deleted'), 'flash_success');  
		}
		
		else {
      $this->Session->setFlash(__('User was not deleted'), 'flash_danger');
    }    
    
    $this->redirect(array('action' => 'user_index'));
  }
  
}